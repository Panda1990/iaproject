package Logic;
import it.unical.mat.embasp.languages.Id;
import it.unical.mat.embasp.languages.Param;


@Id("nextMove")
public class NextMove {
	@Param(0)
	int row;
	@Param(1)
	int column;
	@Param(2)
	int value; //0 vuota 1 blu 2 rosso 3 giallo 4 verde
	@Param(3)
	int newRow;
	@Param(4)
	int newColumn;
	
	public int getRow() {
		return row;
	}
	public void setRow(int row) {
		this.row = row;
	}
	public int getColumn() {
		return column;
	}
	public void setColumn(int column) {
		this.column = column;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public int getNewRow() {
		return newRow;
	}
	public void setNewRow(int newRow) {
		this.newRow = newRow;
	}
	public int getNewColumn() {
		return newColumn;
	}
	public void setNewColumn(int newColumn) {
		this.newColumn = newColumn;
	}
	
	public NextMove(int i, int j, int value, int i2, int j2) {
		this.row = i;
		this.column = j;
		this.value = value;
		this.newRow = i2;
		this.newColumn = j2;
	}
	
	public NextMove () {}
	
	@Override
	public String toString() {
		String print = "Old Position: " + row + ":" + column + "\n" + "New Position: " + newRow + ":" + newColumn + "\n" + "Value: " + value;
		return print;
	}
	
}
