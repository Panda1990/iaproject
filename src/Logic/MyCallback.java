package Logic;
import java.util.LinkedList;
import Graphic.StateHandler;
import it.unical.mat.embasp.base.Callback;
import it.unical.mat.embasp.base.Output;
import it.unical.mat.embasp.languages.asp.AnswerSet;
import it.unical.mat.embasp.languages.asp.AnswerSets;

public class MyCallback implements Callback {
	
	private int[][] matrix;
	LinkedList<PathCell> path;
	
	public MyCallback(int[][] sm, LinkedList<PathCell> path) {
		this.matrix = sm;
		this.path = path;
	}
	

	@Override
	public void callback(Output o) {
		AnswerSets answers = (AnswerSets) o;
		if (!answers.getAnswersets().isEmpty()) {
			AnswerSet a = answers.getAnswersets().get(answers.getAnswersets().size()-1);
			try {
				for(String obj:a.getAnswerSet()){
					if(obj.startsWith("nextMove")) {
						int x1 = Integer.parseInt(obj.substring(9, 10));
						int y1 = Integer.parseInt(obj.substring(11, 12));
						int x2 = Integer.parseInt(obj.substring(15, 16));
						int y2 = Integer.parseInt(obj.substring(17, 18));
						int value = Integer.parseInt(obj.substring(13, 14));
						path = new LinkedList<>();
						//findPath (x1, y1, x2, y2);
						Game.pair = new Pair(x1, y1);
						Game.pair2 = new Pair(x2, y2);
						StateHandler.repaint();
						Thread.sleep(1000);
						System.out.println(obj);
						matrix[x1][y1] = 0;
						matrix[x2][y2] = value;
						break;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public boolean findPath (int x1, int y1, int x2, int y2) {
		if (x1 == x2 && y1 == y2)
			return true;
		
		if (x1 > x2) {
			if (matrix [x1-1] [y1] == 0) {
				path.add(new PathCell (x1-1, y1, 5));
				if (findPath (x1-1, y1, x2, y2) == true)
					return true;
				else 
					path.removeLast();
			}
		}
		
		if (x1 < x2) {
			if (matrix [x1+1] [y1] == 0) {
				path.add(new PathCell (x1+1, y1, 5));
				if (findPath(x1+1, y1, x2, y2) == true)
					return true;
				else
					path.removeLast();
			}
		}
		
		if (y1 < y2) {
			if (matrix [x1] [y1+1] == 0) {
				path.add(new PathCell (x1, y1+1, 6));
				if (findPath(x1, y1+1, x2, y2) == true)
					return true;
				else
					path.removeLast();
			}
		}
		
		if (y1 > y2) {
			if (matrix [x1] [y1-1] == 0) {
				path.add(new PathCell (x1, y1-1, 6));
				if (findPath(x1, y1-1, x2, y2) == true)
					return true;
				else
					path.removeLast();
			}
		}
		
		return false;
	}
	
	
}
