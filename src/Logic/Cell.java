package Logic;
import it.unical.mat.embasp.languages.Id;
import it.unical.mat.embasp.languages.Param;

@Id("cell")
public class Cell {
	
	@Param(0)
	int row;
	@Param(1)
	int column;
	@Param(2)
	int value; //0 vuota 1 blu 2 rosso 3 giallo 4 verde
	
	public Cell(int x, int y, int value) {
		row = x;
		column = y;
		this.value = value;
	}
	
	public Cell() {}
	
	public int getValue() {
		return value;
	}
	
	public void setValue(int value) {
		this.value = value;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}
	
}
