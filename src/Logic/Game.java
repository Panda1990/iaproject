package Logic;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import it.unical.mat.embasp.base.Handler;

public class Game {
	private int[][] matrix;
	int N;
	String encodingResource;
	String encodingGame;
	Handler handler;
	public static Pair pair = null;
	public static Pair pair2 = null;
	Random rndm = new Random();
	ProxCell [] prox = new ProxCell [3];
	public LinkedList<PathCell> path;
	private boolean isEnd = false;
	private List<Pair> posLibere;
	StartGame startGame;
	private Lock lock = new ReentrantLock();
	private boolean already;
	private int score = 0;
	
	public boolean gioca = false;
	public boolean playone = true;
	public Lock lockgame = new ReentrantLock();
	public Condition conditionGame = lockgame.newCondition();

	public Game() {
		N = 9;
		matrix = new int [N][N];
		already = false;
		encodingResource = "encodings/ballLines";
		encodingGame = "lib/ballLines+";
		posLibere = new ArrayList<Pair>();
		path = new LinkedList<>();
		initMatrix();
	}
	
	public ProxCell[] getProxCellArray () {
		return prox;
	}
	
	void initMatrix () {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				matrix[i][j] = 0;
				posLibere.add(new Pair(i, j));
			}
		}
		
		Pair [] initialize = new Pair [5];
		for (int i = 0; i < 5; i++) {
			int itmp = rndm.nextInt(N);
			int jtmp = rndm.nextInt(N);
			initialize [i] = new Pair(itmp, jtmp);
			
			int value = rndm.nextInt(4) + 1;
			matrix[itmp][jtmp] = value;
			posLibere.remove(new Pair(itmp, jtmp));
		}
		
			fillProxBalls();
	}

	void displayMatrix() {
		for(int i = 0; i < N; i++){
			for(int j = 0;j < N; j++){
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
	}
	
	private void resetOrizzontale (int i, int j, int k) {
		for (int l = j; l < j+k; l++) {
			matrix [i][l] = 0;
			posLibere.add(new Pair(i, l));
		}
		refreshScore(k);
	}
	
	private void resetVerticale (int i, int j, int k) {
		for (int k2 = i; k2 < i+k; k2++) {
			matrix [k2][j] = 0;
			posLibere.add(new Pair(k2, j));
		}
		refreshScore(k);
	}
	
	private void resetDiagonaleDxAlto (int i, int j, int k) {
		for (int l = 0; l < k; l++) {
			matrix [i-l][j+l] = 0;
			posLibere.add(new Pair (i-l, j+l));
		}
		refreshScore(k);
	}
	
	private void resetDiagonaleDxBasso (int i, int j, int k) {
		for (int l = 0; l < k; l++) {
			matrix [i+l][j+l] = 0;
			posLibere.add(new Pair (i+l, j+l));
		}
		refreshScore(k);
	}
	
	private void refreshScore (int k) {
		score += k*10;
	}
	
	void refreshMatrix () {
		isEnd = true;
		boolean remove = false;
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {

				int valuetmp = matrix [i][j];
				int occorrenze = 0;
				
				if (valuetmp == 0) {
					isEnd = false;
					posLibere.add(new Pair (i,j));
				} else {
					// Controllo orizzontale
					for (int k = j; k < N; k++) {
						if (matrix [i][k] == valuetmp)
							occorrenze++;
						else
							break;
					}
					
					if (occorrenze >= 5) {
						resetOrizzontale (i, j, occorrenze);
						remove = true;
					}
					occorrenze = 0;
					// Controllo Verticale
					for (int k = i; k < N; k++) {
						if (matrix [k][j] == valuetmp)
							occorrenze++;
						else
							break;
					}
					
					if (occorrenze >= 5) {
						resetVerticale (i, j, occorrenze);
						remove = true;
					}
					occorrenze = 0;
					
					//Controllo Diangonale DX Alto
					for (int k = i, l = j; l < N && k >= 0; k--, l++) {
						if (matrix [k][l] == valuetmp)
							occorrenze++;
						else {
							break;
						}
					}
					
					if (occorrenze >= 5) {
						resetDiagonaleDxAlto(i, j, occorrenze);
						remove = true;
					}
					occorrenze = 0;
					
					// Controllo Diagonale Basso DX
					for (int k = i, l = j; l < N && k < N; k++, l++) {
						if (matrix [k][l] == valuetmp)
							occorrenze++;
						else {
							break;
						}
					}
					
					if (occorrenze >= 5) {
						resetDiagonaleDxBasso(i, j, occorrenze);
						remove = true;
					}
					occorrenze = 0;
				}
			}
		}
		if (!remove && !already) {
			proxInMatrix ();
			already = true;
			refreshMatrix();
			fillProxBalls();
		}
		already = false;
		//path = new LinkedList<>();
	}
	
	void refreshProgram () {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(encodingGame));
			BufferedReader reader = new BufferedReader(new FileReader(encodingResource));
			
			for (int i = 0; i < matrix.length; i++) {
				for (int j = 0; j < matrix.length; j++) {
					if (matrix[i][j] != 0) {
						String fact = ("cell (" + i + "," + j + "," + matrix[i][j] + ").\n");
						writer.append(fact);
					}
				}
			}
			for (int i = 0; i < prox.length; i++) {
				String fact = ("proxCell (" + prox[i].row + "," + prox[i].column + "," + prox[i].value + "). \n");
				writer.append(fact);
			}
				String line = "";
				while ((line = reader.readLine()) != null) {
					writer.append(line);
					writer.append("\n");
				}
				
				writer.flush();
				writer.close();
				reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void proxInMatrix () {
		for (int i = 0; i < prox.length; i++) {
			if (matrix [prox[i].row] [prox[i].column] == 0)
				matrix [prox[i].row] [prox[i].column] = prox[i].value;
		}
	}
	
	void fillProxBalls () {
		for (int j = 0; j < prox.length; j++) {
			if (posLibere.size() > 0) {
				int index = rndm.nextInt(posLibere.size());
				int value = rndm.nextInt(4) + 1;
				
				ProxCell proxCell = new ProxCell(posLibere.get(index).getX(), posLibere.get(index).getY(), value);
				prox[j] = proxCell;
				posLibere.remove(index);
			}
		}
		deleteAllFreePosition();
	}

	private void deleteAllFreePosition () {
		posLibere.clear();
	}
	
	public boolean isEnd () {
		return isEnd;
	}
	
	public void startGame () {
		this.startGame = new StartGame(this);
		startGame.start();
	}
	
	public int getScore () {
		return score;
	}
	
	public int[][] getMatrix (){
		return matrix;
	}

	public void lockMatrix () {
		lock.lock();
	}
	
	public void releaseMatrix() {
		lock.unlock();
	}
	
}
