package Logic;

import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class PlayerAudio extends Thread implements LineListener{
	    boolean playCompleted;
	    boolean effetto = false;
	    String toPath;
	    
	    File audioFile;
	    AudioInputStream audioStream;
	    AudioFormat format;
	    DataLine.Info info;
	    Clip audioClip;
	    
	    public void playCredits (){
	    	playCompleted = true;
	    	toPath = "music/credits.wav";
	    }
	    
	    public void playMenu (){
	    	playCompleted = true;
	    	toPath = "music/menu.wav";
	    }
	     
	    public void playGame (){
	    	playCompleted = true;
	    	toPath = "music/game.wav";
	    }
	    
	    void play() {
	    	playCompleted = false;
	        audioFile = new File(toPath);
	 
	        try {
	            audioStream = AudioSystem.getAudioInputStream(audioFile);
	            format = audioStream.getFormat();
	            info = new DataLine.Info(Clip.class, format);
	            audioClip = (Clip) AudioSystem.getLine(info);
	            audioClip.addLineListener(this);
	            audioClip.open(audioStream);
	            audioClip.start();
	             
	            while (!playCompleted) {
	                try {
	                    Thread.sleep(1000);
	                    if (audioClip.getFramePosition() >= audioClip.getFrameLength() - 10) 
	                    	playCompleted = true;
	                } catch (InterruptedException ex) {
	                    ex.printStackTrace();
	                }
	            }
	            audioClip.close();
	             
	        } catch (UnsupportedAudioFileException ex) {
	            System.out.println("The specified audio file is not supported.");
	            ex.printStackTrace();
	        } catch (LineUnavailableException ex) {
	            System.out.println("Audio line for playing back is unavailable.");
	            ex.printStackTrace();
	        } catch (IOException ex) {
	            System.out.println("Error playing the audio file.");
	            ex.printStackTrace();
	        }
	         
	    }
	    
	    @Override
	    public void update(LineEvent event) {
	       
	    }

		@Override
		public void run() {
			while (true)
				this.play();
		}
}
