package Logic;

public class PathCell {
	Pair pair;
	int value;
	
	public PathCell(int x1, int y1, int value) {
		pair = new Pair(x1, y1);
		this.value = value;
	}

	public Pair getPair() {
		return pair;
	}

	public void setPair(Pair pair) {
		this.pair = pair;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
	
	
}
