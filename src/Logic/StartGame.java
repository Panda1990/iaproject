package Logic;
import Graphic.StateHandler;
import it.unical.mat.embasp.base.InputProgram;
import it.unical.mat.embasp.base.Output;
import it.unical.mat.embasp.languages.asp.ASPInputProgram;
import it.unical.mat.embasp.platforms.desktop.DesktopHandler;
import it.unical.mat.embasp.specializations.dlv.desktop.DLVDesktopService;

public class StartGame extends Thread {
	Game game;
	MyCallback call;
	InputProgram facts;
	int iteration = 0;
	
	public StartGame(Game game) {
		this.game = game;
		game.handler = new DesktopHandler(new DLVDesktopService("lib/dlv2"));
		facts = new ASPInputProgram();
	}
	
	
	@Override
	public void run() {
		super.run();
		StateHandler.repaint();
		int [] [] matrix = game.getMatrix();
		this.call = new MyCallback(matrix, game.path);
		game.refreshProgram();
		InputProgram encoding = new ASPInputProgram();
		encoding.addFilesPath(game.encodingGame);		
		game.handler.addProgram(encoding);
		
		while (!game.isEnd () && StateHandler.getInstance().state == 1) {
			game.lockMatrix(); // prendo il lock, non disegnare
			
			Output o =  game.handler.startSync();
			call.callback(o);
			
			//Preparazione prossimo turno
			game.refreshMatrix();
			game.releaseMatrix(); // rilascia lock matrice, possibile disegnare adesso
			game.refreshProgram (); // aggiorna sul file

			try {
				sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			StateHandler.repaint();
			game.lockgame.lock();
			while (game.gioca == false) {
				try {
					game.conditionGame.await();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (game.playone)
				game.gioca = false;
			game.lockgame.unlock();
		}
	}
	
}
