package Graphic;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class MyFrame extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private StatePanel state;
	private int height;
	private int width;
	
	public MyFrame(StatePanel pane)
	{
		this.height = 700;
		this.width = 1200;
		this.state = pane;
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(this.width, this.height);
		setResizable(true);
		setVisible(true);
		setLocation(10, 10);
		this.getContentPane().add((JPanel) state);
		setResizable(false);
	}
	
	public void setJPanel(JPanel p)
	{
		this.getContentPane().removeAll();
		this.getContentPane().add(p);
		this.revalidate();
		this.getContentPane().repaint();
	}
	
	public static void main(String[] args) {
		Loader.getInstance().loadMenu();
	    Loader.getInstance().start();
	    StateHandler.getInstance();
	}

}
