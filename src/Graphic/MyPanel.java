package Graphic;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	
	 private static final long serialVersionUID = 1L;
	 StartButton sb = null;
	 CreditsButton cb = null;
	 int state = 0; // 0 menu, 1 gioco, 2 credits
	 
	  public MyPanel()
	 
	  {
			sb = new StartButton();
		    cb = new CreditsButton();
		    this.setLayout(null);
		    add(sb);	 
		    add(cb);	 
		    //setto startbutton	 
		    sb.setVisible(true);	 
		    sb.setBorderPainted(false);	 
		    sb.setBorder(BorderFactory.createEmptyBorder());	 
		    sb.setBounds(10, 10, sb.getWidth(), sb.getHeight());	 
		    java.awt.Insets insets = this.getInsets();	 
		    Dimension size = sb.getPreferredSize();	 
		    sb.setBounds(150 + insets.left, 300 + insets.top, size.width, size.height); // absolute position	 
		    //setto credistbutton	 
		    cb.setVisible(true);	 
		    cb.setBorderPainted(false);	 
		    cb.setBorder(BorderFactory.createEmptyBorder());	 
		    cb.setBounds(10, 10, sb.getWidth(), sb.getHeight());	 
		    java.awt.Insets insets2 = this.getInsets();	 
		    Dimension size2 = sb.getPreferredSize();	 
		    cb.setBounds(150 + insets2.left, 450 + insets2.top, size2.width, size2.height);	 
	  }
	 
	@Override
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
		if (this.state == 0)
		{
			
			sb.setVisible(true);
			sb.setEnabled(true);
			cb.setVisible(true);
			cb.setEnabled(true);
			g2d.drawImage(Loader.getInstance().getSfondo(), 0, 0, this);
		}
	}
}
