package Graphic;

import java.awt.Image;

import java.awt.Toolkit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
 

 
public class Loader extends Thread {
 
  Toolkit tk;
  Image sfondo;
  Image start_button_positive;
  Image credits_button;
  Image piana;
  Image credits;
  Image back_to_menu_button;
  Image [] ballImages = new Image [6];
  Image [] ballImagesTrasp = new Image [4];
  Image [] proxBall = new Image [4];
  Image toMove;
  Image playOneButton;
  Image playRapidButton;
  Lock lockGame;
  Lock lockCredits;
  Condition conditionGame;
  Condition conditionCredits;
  
  private static Loader instance = null;
 
  private Loader() {
	    tk = Toolkit.getDefaultToolkit();
	    lockGame = new ReentrantLock();
	    lockCredits = new ReentrantLock();
	    conditionGame = lockGame.newCondition();
	    conditionCredits = lockCredits.newCondition();
  }
 
	public static Loader getInstance()
	{
		if (instance == null)
		{
			instance = new Loader();
		}
		return instance;
	}
	
	// Menu Loader
	
	private void loadSfondo()
	{
		this.sfondo = tk.getImage("images/sfondo.jpg");
	}
	
	 public Image getSfondo()
	  {
	    return this.sfondo;
	  }
	 
	  private void loadStartButton()
	  {
	    this.start_button_positive = tk.getImage("images/start_button.png");
	  }
	 
	  public Image getStartButtonPositive()
	  {
	    return this.start_button_positive;
	  }

	  private void loadCreditsButton()
	  {
	    this.credits_button = tk.getImage("images/credits_button.png");
	  }
	  public Image getCreditsButton()
	  {
	    return this.credits_button;
	  }
	  
	  public void loadMenu () {
		    loadSfondo();
		    loadStartButton();
		    loadCreditsButton();
	  }
	  
	  // End Menu Loader
	  
	  // Game Loader
	  
	  private void loadPiana()
	  {
		  this.piana = tk.getImage("images/piana.jpg");
	  }
	  
	  public Image getPiana()
	  {
		  try {
			lockGame.lock();
			while (this.piana == null)
				try {
					conditionGame.await();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			  return this.piana;
		} finally {
			lockGame.unlock();
		}
	  }
	  
	  private void loadBackToMenuButton()
	  {
		  this.back_to_menu_button = tk.getImage("images/back_button.png");
	  }
	  
	  public Image getBackToMenuButton()
	  {

		  try {
			lockGame.lock();
			while (this.back_to_menu_button == null)
				try {
					conditionGame.await();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			  return this.back_to_menu_button;
		} finally {
			lockGame.unlock();
		}
	  }
	  
	  private void loadPlayOneButton()
	  {
		  this.playOneButton = tk.getImage("images/playOneButton.png");
	  }
	  
	  public Image getPlayOneButton()
	  {

		  try {
			lockGame.lock();
			while (this.playOneButton == null)
				try {
					conditionGame.await();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			  return this.playOneButton;
		} finally {
			lockGame.unlock();
		}
	  }
	  
	  private void loadPlayRapidButton()
	  {
		  this.playRapidButton = tk.getImage("images/playRapidButton.png");
	  }
	  
	  public Image getPlayRapidButton()
	  {

		  try {
			lockGame.lock();
			while (this.playRapidButton == null)
				try {
					conditionGame.await();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			  return this.playRapidButton;
		} finally {
			lockGame.unlock();
		}
	  }
	  
	  private void loadBall () {
		  ballImages [0] = tk.getImage("images/blueball.png");
		  ballImages [1] = tk.getImage("images/redball.png");
		  ballImages [2] = tk.getImage("images/yellowball.png");
		  ballImages [3] = tk.getImage("images/greenball.png");
		  ballImages [4] = tk.getImage("images/horLine.png");
		  ballImages [5] = tk.getImage("images/verLine.png");
	  }
	  
	  public Image getBall (int index) {

		  try {
			lockGame.lock();
			while (ballImages[3] == null)
				try {
					conditionGame.await();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			  return ballImages [index-1];
		} finally {
			lockGame.unlock();
		}
	  }
	  
	  private void loadTraspBall () {
		  ballImagesTrasp [0] = tk.getImage("images/blueballT.png");
		  ballImagesTrasp [1] = tk.getImage("images/redballT.png");
		  ballImagesTrasp [2] = tk.getImage("images/yellowballT.png");
		  ballImagesTrasp [3] = tk.getImage("images/greenballT.png");
	  }
	  
	  public Image getTraspBall (int index) {

		  try {
			lockGame.lock();
			while (ballImagesTrasp[3] == null)
				try {
					conditionGame.await();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			  return ballImagesTrasp [index-1];
		} finally {
			lockGame.unlock();
		}
	  }
	  
	  private void loadProxBall () {
		  proxBall[0] = tk.getImage("images/animationBlu.png");
		  proxBall[1] = tk.getImage("images/animationRed.png");
		  proxBall[2] = tk.getImage("images/animationYellow.png");
		  proxBall[3] = tk.getImage("images/animationGreen.png");
	  }
	  
	  public Image getProxBall (int index) {
		  return proxBall[index-1];
	  }
	  
	  public void loadToMove () {
		  toMove = tk.getImage("images/pedinaDaSpostare.png");
	  }
	  
	  public Image getToMove () {
		  return toMove;
	  }
	  
	  private void loadGameState () {
		  try {
			lockGame.lock();
		    loadPiana();
		    loadBackToMenuButton();
		    loadPlayOneButton();
		    loadPlayRapidButton();
		    loadBall();
		    loadTraspBall();
		    loadProxBall();
		    loadToMove();
		    conditionGame.signalAll();
		} finally {
			lockGame.unlock();
		}
	  }
	  
	  // End Game Loader
	  // Credits Loader
	  
	  private void loadCredits()
	  {
		  this.credits = tk.getImage("images/credits_sfondo.jpg");
	  }
	  
	  public Image getCredits()
	  {
		  try {
			lockCredits.lock();
			while (credits == null)
				try {
					conditionCredits.await();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			  return this.credits;
		} finally {
			lockCredits.unlock();
		}
	  }
	  
	  private void loadCreditsState () {
		  try {
			  lockCredits.lock();
			  loadCredits();
			  conditionCredits.signalAll();
		} finally {
			lockCredits.unlock();
		}
	  }
	  
	  // Load Thread
	  
	  @Override
	  public void run() {
		  loadGameState();
		  loadCreditsState();
	  }
}
