package Graphic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class BackToMenuButton extends JButton{

	private static final long serialVersionUID = 1L;
	
	public BackToMenuButton()
	{
		this.setIcon(new ImageIcon(Loader.getInstance().getBackToMenuButton()));
		this.setBorderPainted(false);
		this.setOpaque(false);
		this.setContentAreaFilled(false);
		this.setBorderPainted(false);
		this.setBorder(BorderFactory.createEmptyBorder());
		this.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				StateHandler.getInstance().setPanel(0);
			}
		});
		
	}
}
