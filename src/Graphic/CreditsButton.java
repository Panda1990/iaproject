package Graphic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class CreditsButton extends JButton {

	private static final long serialVersionUID = 1L;
	
	public CreditsButton()
	{
		this.setIcon(new ImageIcon(Loader.getInstance().getCreditsButton()));
		this.setBorderPainted(false);
		this.setBorder(BorderFactory.createEmptyBorder());
		this.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				StateHandler.getInstance().setPanel(2);
			}
		});
		
	}
}
