package Graphic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import Logic.Game;

public class PlayRapidButton extends JButton{

	private static final long serialVersionUID = 1L;
	Game game;
	
	public PlayRapidButton(Game games)
	{
		this.setIcon(new ImageIcon(Loader.getInstance().getPlayRapidButton()));
		this.setBorderPainted(false);
		this.setOpaque(false);
		this.setContentAreaFilled(false);
		this.setBorderPainted(false);
		this.setBorder(BorderFactory.createEmptyBorder());
		this.game = games;
		this.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				game.lockgame.lock();
				game.gioca = true;
				game.playone = false;
				game.conditionGame.signalAll();
				game.lockgame.unlock();
			}
		});
		
	}
}