package Graphic;

import javax.swing.JPanel;

import Logic.PlayerAudio;

public class StateHandler {

	private MyFrame frame;
	private PlayerAudio player;
	private StatePanel statePane;
	private static StateHandler instance = null;
	public int state;
	
	private StateHandler()
	{
		this.statePane = new StateMenu();
		this.frame = new MyFrame(statePane);
		this.player = new PlayerAudio();
		player.playMenu();
		player.start();
		state = 0;
	}
	
	public static void repaint () {
		StateHandler.getInstance().statePane.paint();
	}
	
	public static StateHandler getInstance()
	{
		if (instance == null)
		{
			instance = new StateHandler();
		}
		return instance;
	}
	
	void setPanel(int state)
	{
		this.state = state;
		switch(state)
		{
		case 0: 
			statePane = new StateMenu();
			player.playMenu();
			break;
			
		case 1: 
			statePane = new StateGame();
			player.playGame();
			Thread thread = new Thread((Runnable) statePane);
			thread.start();
			break;
			
		case 2:
			statePane = new StateCredits();
			player.playCredits();
			break;
		
		default: break;
		}
		
		frame.setJPanel((JPanel) statePane);
	}
}
