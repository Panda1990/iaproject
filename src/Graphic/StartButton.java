package Graphic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class StartButton extends JButton{

	private static final long serialVersionUID = 1L;
	
	public StartButton()
	{
		this.setIcon(new ImageIcon(Loader.getInstance().getStartButtonPositive()));
		this.setBorderPainted(false);
		this.setBorder(BorderFactory.createEmptyBorder());
		this.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("ciao mario");
				StateHandler.getInstance().setPanel(1);
			}
		});
		
	}
	
}
