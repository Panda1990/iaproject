package Graphic;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import Logic.Game;
import Logic.ProxCell;

public class StateGame extends JPanel implements StatePanel, Runnable {

	private static final long serialVersionUID = 1L;
	BackToMenuButton sb = null;
	PlayOneButton playbutton = null;
	PlayRapidButton rapidbutton = null;
	Game game;
	int [] [] matrix;
	ProxCell [] prox;
	 
	  public StateGame()
	  {
		  game = new Game();
		  this.matrix = game.getMatrix();
		  prox = game.getProxCellArray();
		  sb = new BackToMenuButton();
		  playbutton = new PlayOneButton(game);
		  rapidbutton = new PlayRapidButton(game);
		  this.setLayout(null);
		  add(sb);
		  add(playbutton);
		  add(rapidbutton);
		  
		  sb.setVisible(true);	
		  playbutton.setVisible(true);
		  rapidbutton.setVisible(true);
		  
		  sb.setBorderPainted(false);	
		  playbutton.setBorderPainted(false);
		  rapidbutton.setBorderPainted(false);
		  
		  sb.setBorder(BorderFactory.createEmptyBorder());	 
		  playbutton.setBorder(BorderFactory.createEmptyBorder());	
		  rapidbutton.setBorder(BorderFactory.createEmptyBorder());	
		  
		  sb.setBounds(10, 10, sb.getWidth(), sb.getHeight());	 
		  playbutton.setBounds(10, 10, playbutton.getWidth(), playbutton.getHeight());	
		  rapidbutton.setBounds(10, 10, rapidbutton.getWidth(), rapidbutton.getHeight());	
		  
		  java.awt.Insets insets = this.getInsets();	
		  
		  Dimension size = sb.getPreferredSize();	 
		  sb.setBounds(730 + insets.left, 480 + insets.top, size.width, size.height); // absolute position	 
		  size = playbutton.getPreferredSize();
		  playbutton.setBounds(730 + insets.left, 350 + insets.top, size.width, size.height);
		  size = rapidbutton.getPreferredSize();
		  rapidbutton.setBounds(850 + insets.left, 350 + insets.top, size.width, size.height);
		  
		  repaint();
	  }
	 
	@Override
	public void paint() {
		this.repaint();
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
		g2d.drawImage(Loader.getInstance().getPiana(), 0, 0, this);
		//game.lockMatrix();
		for (int i = 0; i < prox.length; i++) {
			g2d.drawImage(Loader.getInstance().getProxBall(prox[i].getValue()), 70 * (prox[i].getRow()) + 40, 70 * (prox[i].getColumn()) + 40, this);
		}
		
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				if (matrix [i][j] != 0)
					g2d.drawImage(Loader.getInstance().getBall(matrix[i][j]), 70 * (i) + 40, 70 * (j) + 40, this);
			}
		}
		
		if (game.pair != null && game.pair2 != null && matrix[game.pair.getX()][game.pair.getY()] > 0) {
			g2d.drawImage(Loader.getInstance().getToMove(), 70 * (game.pair.getX()) + 30, 70 * (game.pair.getY()) + 30, this);
			g2d.drawImage(Loader.getInstance().getBall(matrix[game.pair.getX()][game.pair.getY()]), 70 * (game.pair.getX()) + 40, 70 * (game.pair.getY()) + 40, this);
			g2d.drawImage(Loader.getInstance().getTraspBall(matrix[game.pair.getX()][game.pair.getY()]), 70 * (game.pair2.getX()) + 40, 70 * (game.pair2.getY()) + 40, this);
		}
		
		/*for (Iterator it = game.path.iterator(); it.hasNext();) {
			System.out.println("Iterator");
			PathCell cell = (PathCell) it.next();
			g2d.drawImage(Loader.getInstance().getBall(cell.getValue()), 70 * (cell.getPair().getX()) + 40, 70 * (cell.getPair().getY()) + 40, this);
		}*/
		
		g.setFont(new Font("Lobster Two", Font.BOLD, 32));
		g2d.drawString("Score: " + game.getScore (), 750, 300);
	}

	@Override
	public void run() {
		repaint();
		game.startGame();
	}
}
