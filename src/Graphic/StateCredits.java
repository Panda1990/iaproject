package Graphic;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class StateCredits extends JPanel implements StatePanel {

	private static final long serialVersionUID = 1L;
	BackToMenuButton sb = null;
	 
	  public StateCredits()
	  {
			sb = new BackToMenuButton();
		    this.setLayout(null);
		    add(sb);	  
		    //setto startbutton	 
		    sb.setVisible(true);	 
		    sb.setBorderPainted(false);	 
		    sb.setBorder(BorderFactory.createEmptyBorder());	 
		    sb.setBounds(10, 10, sb.getWidth(), sb.getHeight());	 
		    java.awt.Insets insets = this.getInsets();	 
		    Dimension size = sb.getPreferredSize();	 
		    sb.setBounds(900 + insets.left, 450 + insets.top, size.width, size.height); // absolute position	 	
		    
	  }
	 
	@Override
	public void paint() {
		this.repaint();
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
		g2d.drawImage(Loader.getInstance().getCredits(), 0, 0, this);
	}
}
